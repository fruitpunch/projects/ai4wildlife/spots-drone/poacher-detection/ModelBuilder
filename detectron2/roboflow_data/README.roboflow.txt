
AI for Wildlife 1000 - v1 2021-05-04 9:07pm
==============================

This dataset was exported via roboflow.ai on May 4, 2021 at 7:08 PM GMT

It includes 982 images.
Humans are annotated in COCO format.

The following pre-processing was applied to each image:

No image augmentation techniques were applied.


