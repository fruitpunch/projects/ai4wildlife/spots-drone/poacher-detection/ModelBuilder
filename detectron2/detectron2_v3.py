# -*- coding: utf-8 -*-

# import some common libraries
import numpy as np
import os
import pandas as pd
from collections import OrderedDict
import cv2
import matplotlib.pyplot as plt
import random
import pickle
import torch
import time
import datetime
import logging
import argparse
import glob
import sys
# import some common detectron2 utilities
from detectron2.utils.logger import setup_logger
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data.datasets import register_coco_instances
from detectron2.engine import DefaultTrainer
from detectron2.engine.hooks import HookBase
from detectron2.utils.logger import log_every_n_seconds
from detectron2.data import DatasetMapper, build_detection_test_loader
import detectron2.utils.comm as comm
from detectron2.data import DatasetCatalog, MetadataCatalog, build_detection_test_loader
from detectron2.evaluation import COCOEvaluator, inference_on_dataset
from detectron2.evaluation import print_csv_format

# Setup detectron2 logger
setup_logger()

# Input arguments
parser = argparse.ArgumentParser(description='Detectron2')
parser.add_argument('--train_images', type=str, required=True, help='training images to use')
parser.add_argument('--val_images', type=str, required=True, help='validation images to use')
parser.add_argument('--test_images', type=str, required=True, help='test images to use')
parser.add_argument('--train_labels', type=str, required=True, help='training annotations to use')
parser.add_argument('--val_labels', type=str, required=True, help='validation annotations to use')
parser.add_argument('--test_labels', type=str, required=True, help='test annotations to use')
parser.add_argument('--output_dir', type=str, help='where to save the output image')
parser.add_argument('--nEpochs', type=int, default=2, help='number of epochs to train for')
parser.add_argument('--lr', type=float, default=0.001, help='Learning Rate. Default=0.01')
parser.add_argument('--cuda', action='store_true', help='use cuda?')
# parser.add_argument('--threads', type=int, default=4, help='number of threads for data loader to use')
parser.add_argument('--seed', type=int, default=123, help='random seed to use. Default=123')
opt = parser.parse_args()

sys.stdout = open(opt.output_dir + '/print_output', 'w')

print('####################')
print("torch version:::", torch.__version__)
print("Input arguments:::")
print(opt)

cuda = opt.cuda
if cuda and not torch.cuda.is_available():
    raise Exception("No GPU found, please run without --cuda")
torch.manual_seed(opt.seed)
if cuda:
    torch.cuda.manual_seed(opt.seed)

"""Register COCO format dataset
If the instance-level (detection, segmentation, keypoint) dataset is already a json file in the COCO format, the dataset
 and its associated metadata can be registered easily with register_coco_instances method
"""

# Register dataset
register_coco_instances("wildlife_dataset_train", {}, opt.train_labels, opt.train_images)
register_coco_instances("wildlife_dataset_val", {}, opt.val_labels, opt.val_images)
register_coco_instances("wildlife_dataset_test", {}, opt.test_labels, opt.test_images)

"""
#Data loader
The data loader of Detectron 2 is multi-level nested. It is built by the builder before starting training.
    dataset_dicts (list) is a list of annotation data registered from the dataset.
    DatasetFromList (data.Dataset) takes a dataset_dicts and wrap it as a torch dataset.
    MapDataset (data.Dataset) calls DatasetMapper class to map each element of DatasetFromList. It loads images, 
    transforms images and annotations, and converts annotations to an ‘Instances’ object.

#Predefined tranformation used before training:
    d2.data.dataset_mapper: [DatasetMapper] Augmentations used in training: 
    [ResizeShortestEdge(short_edge_length=(640, 672, 704, 736, 768, 800), max_size=1333, sample_style='choice'), 
    RandomFlip()]
"""

# Visualize training data
my_dataset_train_metadata = MetadataCatalog.get("wildlife_dataset_train")
dataset_dicts = DatasetCatalog.get("wildlife_dataset_train")

counter = 0
for d in random.sample(dataset_dicts, 3):
    counter = counter + 1
    img = cv2.imread(d["file_name"])
    visualizer = Visualizer(img[:, :, ::-1], metadata=my_dataset_train_metadata, scale=0.5)
    vis = visualizer.draw_dataset_dict(d)
    cv2.imwrite(opt.output_dir + "/training_images_" + str(counter) + ".png", vis.get_image()[:, :, ::-1])

print("########################################Configuration############################")
cfg = get_cfg()
print(cfg)
print("##################################################################################")
cfg.merge_from_file(model_zoo.get_config_file("COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x.yaml"))
cfg.DATASETS.TRAIN = ("wildlife_dataset_train",)
cfg.DATASETS.TEST = ("wildlife_dataset_val",)
cfg.DATALOADER.NUM_WORKERS = 4
cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(
    "COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x.yaml")  # Let training initialize from model zoo

# Solver options
cfg.SOLVER.BASE_LR = opt.lr  # Base learning rate
cfg.SOLVER.IMS_PER_BATCH = 1  # Lower to reduce memory usage (1 is the lowest)
cfg.SOLVER.WARMUP_ITERS = 100  # Warmup iterations to linearly ramp learning rate from zero
cfg.SOLVER.MAX_ITER = opt.nEpochs  # Maximum number of iterations \
# Adjust up if val mAP is still rising, adjust down if overfit
cfg.SOLVER.STEPS = (100, 200)  # Iterations at which to decay learning rate
cfg.SOLVER.GAMMA = 0.5  # Learning rate decay # previous 0.05

cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 64
cfg.MODEL.ROI_HEADS.NUM_CLASSES = 2
cfg.TEST.EVAL_PERIOD = 20  # evaluation once after 20 iterations on the cfg.DATASETS.TEST, which is the Validation set


# Validation loss calculation


class LossEvalHook(HookBase):
    def __init__(self, eval_period, model, data_loader):
        print("####Validation Loss Calculation####")
        self._model = model
        self._period = eval_period
        self._data_loader = data_loader

    def _do_loss_eval(self):
        # Copying inference_on_dataset from evaluator.py
        total = len(self._data_loader)
        num_warmup = min(5, total - 1)

        start_time = time.perf_counter()
        total_compute_time = 0
        losses = []
        for idx, inputs in enumerate(self._data_loader):
            if idx == num_warmup:
                start_time = time.perf_counter()
                total_compute_time = 0
            start_compute_time = time.perf_counter()
            if torch.cuda.is_available():
                torch.cuda.synchronize()
            total_compute_time += time.perf_counter() - start_compute_time
            iters_after_start = idx + 1 - num_warmup * int(idx >= num_warmup)
            seconds_per_img = total_compute_time / iters_after_start
            if idx >= num_warmup * 2 or seconds_per_img > 5:
                total_seconds_per_img = (time.perf_counter() - start_time) / iters_after_start
                eta = datetime.timedelta(seconds=int(total_seconds_per_img * (total - idx - 1)))
                log_every_n_seconds(
                    logging.INFO,
                    "Loss on Validation done {}/{}. {:.4f} s / img. ETA={}".format(
                        idx + 1, total, seconds_per_img, str(eta)
                    ),
                    n=5,
                )
            loss_batch = self._get_loss(inputs)
            losses.append(loss_batch)
        mean_loss = np.mean(losses)
        self.trainer.storage.put_scalar('validation_loss', mean_loss)
        comm.synchronize()

        return losses

    def _get_loss(self, data):
        # How loss is calculated on train_loop
        metrics_dict = self._model(data)
        metrics_dict = {
            k: v.detach().cpu().item() if isinstance(v, torch.Tensor) else float(v)
            for k, v in metrics_dict.items()
        }
        total_losses_reduced = sum(loss for loss in metrics_dict.values())
        return total_losses_reduced

    def after_step(self):
        # Early stopping can be implemented here
        logger = logging.getLogger("detectron2")
        results = OrderedDict()
        for dataset_name in cfg.DATASETS.TEST:
            data_loader = build_detection_test_loader(cfg, dataset_name)
            evaluator = COCOEvaluator("wildlife_dataset_val", cfg, False, output_dir="./output/")
            results_i = inference_on_dataset(self._model, data_loader, evaluator)
            results[dataset_name] = results_i
            if comm.is_main_process():
                logger.info("Evaluation results for {} in csv format:".format(dataset_name))
                print_csv_format(results_i)
        if len(results) == 1:
            results = list(results.values())[0]
            print("Results:::")
            print(results)
        next_iter = self.trainer.iter + 1
        is_final = next_iter == self.trainer.max_iter
        if is_final or (self._period > 0 and next_iter % self._period == 0):
            self._do_loss_eval()
        self.trainer.storage.put_scalars(timetest=12)


class CocoTrainer(DefaultTrainer):
    @classmethod
    def build_evaluator(cls, cfg, dataset_name, output_folder=None):
        print("####Inside build_evaluator method####")
        if output_folder is None:
            output_folder = os.path.join(cfg.OUTPUT_DIR, "inference")  # same as coco_eval
        return COCOEvaluator(dataset_name, cfg, True, output_folder)

    def build_hooks(self):
        print("####Inside build_hooks method####")
        hooks = super().build_hooks()
        hooks.insert(-1, LossEvalHook(
            cfg.TEST.EVAL_PERIOD,
            self.model,
            build_detection_test_loader(
                self.cfg,
                self.cfg.DATASETS.TEST[0],
                DatasetMapper(self.cfg, True)
            )
        ))
        return hooks


print("Current path:::", os.getcwd())
arr = os.listdir('.')
print("List of files in current directory:::", arr)
print("cfg.OUTPUT_DIR:::", cfg.OUTPUT_DIR)
os.makedirs(cfg.OUTPUT_DIR, exist_ok=True)
trainer = CocoTrainer(cfg)
trainer.resume_or_load(resume=False)
trainer.train()

# Evaluation using the trained model on testset

output_path = cfg.OUTPUT_DIR
print("Files in output directory:::", os.listdir(output_path))

cfg.MODEL.WEIGHTS = os.path.join(cfg.OUTPUT_DIR, "model_final.pth")
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.91  # set the testing threshold for this model

evaluator = COCOEvaluator("wildlife_dataset_test", cfg, False, output_dir="./output/")
test_loader = build_detection_test_loader(cfg, "wildlife_dataset_test")
eval_results = inference_on_dataset(trainer.model, test_loader, evaluator)
print("Evaluation results:::")
print(eval_results)

# Saving the current configuration file
with open(cfg.OUTPUT_DIR + "cfg.pkl", "wb") as f:
    pickle.dump(cfg, f)

# Inference using the trained model on testset

print("############Inference##############")

test_metadata = MetadataCatalog.get("wildlife_dataset_test")
predictor = DefaultPredictor(cfg)

counter = 0
instances = []
for imageName in glob.glob(opt.test_images + '/*jpg'):
    counter = counter + 1
    im = cv2.imread(imageName)
    outputs = predictor(im)
    v = Visualizer(im[:, :, ::-1],
                   metadata=test_metadata,
                   scale=0.8
                   )
    l = len(outputs["instances"])
    instances.append(l)
    out = v.draw_instance_predictions(outputs["instances"].to("cpu"))
    cv2.imwrite(opt.output_dir + "/inference" + str(counter) + ".png", out.get_image()[:, :, ::-1])

print("Number of test images:", len(instances))
print("Human instances found in all test images:", instances)

s = sum(instances)
print("Total number of instances found in all images:::", s)

# Metrics calculation
print("#######Metrics##########")

metrics_df = pd.read_json("./output/metrics.json", orient="records", lines=True)
mdf = metrics_df.sort_values("iteration")
print(mdf)
mdf.to_csv('./output/metrics.csv')

# Plot loss curve
fig, ax = plt.subplots()

mdf1 = mdf[~mdf["total_loss"].isna()]
ax.plot(mdf1["iteration"], mdf1["total_loss"], c="C0", label="train")
if "validation_loss" in mdf.columns:
    mdf2 = mdf[~mdf["validation_loss"].isna()]
ax.plot(mdf2["iteration"], mdf2["validation_loss"], c="C1", label="validation")

# ax.set_ylim([0, 0.5])
ax.legend()
ax.set_title("Loss curve")
# plt.show()
plt.savefig("./output/val_vs_train_loss.png")

# AP75 plot
fig, ax = plt.subplots()
mdf3 = mdf[~mdf["bbox/AP75"].isna()]
ax.plot(mdf3["iteration"], mdf3["bbox/AP75"] / 100., c="C2", label="validation")
ax.legend()
ax.set_title("AP75")
# plt.show()
plt.savefig("./output/AP75.png")

sys.stdout.close()
