## Introduction
Hello everyone, we will use this shared repository to build a rhino poaching detection model

Since we will be collaborating together on the code, it is preferred to set some guidelines for the repo:
- When developing a model, please create a branch from the master branch. Please create merge requests when pushing your code/model to the master branch. In Gitlab you can assign members of the team to accept/reject the merge.
- I have added a gitignore on the Data folder
- Please list your packages and their version in the requirements.txt file
- Maybe good to keep a standard way of coding? Variable names start with lowercases,  capitalize the following words within the variable name so: thisIsAnExampleVariable
- for definitions/methods/classes use capital letters
- Please feel free to add any further guidelines 

## Folder Structure

In progress
- Code
- Notebooks
- SampleData