
import h5py
import numpy as np
from spair import config as cfg
import torch
from torch.utils import data
import cv2
import os

class SimpleScatteredMNISTDataset(torch.utils.data.Dataset):
    def __init__(self, in_file):
        super().__init__()
        self.dataset = h5py.File(in_file, 'r')['train/full']
        self.episode = None

        # static_img = self.dataset[9, ...]
        # img_size = cfg.INPUT_IMAGE_SHAPE[-1]
        # self.static_img = cv2.resize(static_img, dsize=(img_size,img_size))

    def __getitem__(self, index):
        ret = []

        obs = self.dataset['image'][index, ...] # TODO index fixed to 0
        # obs = self.static_img
        # obs = np.zeros_like(obs)
        obs = obs[..., None]  # Add channel dimension
        image = np.moveaxis(obs, -1, 0)  # move from (x, y, c) to (c, x, y)

        bbox = self.dataset['bbox'][index, ...]  # TODO index fixed to 0

        digit_count = self.dataset['digit_count'][index, ...]

        return image, bbox, digit_count

    def __len__(self):
        return self.dataset['image'].shape[0]

import torch
from PIL import Image


class YourImageDataset(torch.utils.data.Dataset):
    def __init__(self, image_folder):
        self.image_folder = image_folder
        self.images = os.listdir(image_folder)

    # get sample
    def __getitem__(self, idx):
        image_file = self.images[idx]

        image = Image.open((self.image_folder + image_file))
        image = np.array(image)
        
        # normalize image
        image = image / 255

        # convert to tensor
        image = torch.Tensor(image).reshape(3, 512, 512)
        
        # get the label, in this case the label was noted in the name of the image file, ie: 1_image_28457.png where 1 is the label and the number at the end is just the id or something
        target = int(image_file.split("_")[0])
        target = torch.Tensor(target)

        return image, target

    def __len__(self):
        return len(self.images)
