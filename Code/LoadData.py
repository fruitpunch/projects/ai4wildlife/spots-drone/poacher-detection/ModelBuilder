import cv2
import os

class Rhino:
    """
    Describing the class
    """
    def __init__(self, length : int, height : int):
        self.lengthRhino = length
        self.height = height


def LoadSampleRhino(data : str) -> Rhino: 
    """
    Define your def with a description in this section here
    """
    length = 200
    height = 80
    writeVariablesLikeThis = True
    
    #Create a rhino object
    rhino = Rhino(length = length, height = height)
    
    return rhino


def load_video_to_images(video_name):
    """
    Loads video file into images using OpenCV and loads the files into folder called image_data in the current directory.
    Tested with mpg and mp4 files.
    Inputs:
        video_name: Name of the video file in the same directory as the python script. type: String
    """
    # Load the video file with OpenCV.
    current_dir = os.getcwd()
    vid_path = os.path.join(current_dir, video_name)
    vidcap = cv2.VideoCapture(vid_path)
    success, image = vidcap.read() # Returns success = True if there is another image to load. image = image file.
    count = 1
    # Make sure that a folder is created for the video
    image_folder = os.path.join(os.path.abspath(current_dir), 'data', 'images')
    if not os.path.exists(image_folder):
        os.makedirs(image_folder)
    while success:
        image_path = os.path.join(image_folder, 'image_' + str(count) + '.jpg')
        cv2.imwrite(image_path, image)
        success, image = vidcap.read()
        print('Saved image ', count)
        count += 1


if __name__ == '__main__':
    data = "someData"
    LoadSampleRhino(data)